package com.example.test.api.controller;

import com.example.test.api.model.Product;
import com.example.test.api.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ProductController {

    @Autowired
    ProductRepository repository;

    @GetMapping("/api/products")
    public Iterable<Product> getProducts() {
        return repository.findAll();
    }

    @PostMapping("/api/products")
    public ResponseEntity createProducts(@RequestBody Product product) {
        repository.save(product);
        return new ResponseEntity("Successfully created ", new HttpHeaders(), HttpStatus.CREATED);
    }

    @PutMapping("/api/products/{id}")
    public ResponseEntity updateProduct(@PathVariable("id") int id, @RequestBody Product product) {
        Product updateProduct = repository.getById(id);
        if (updateProduct == null)
            return new ResponseEntity("Product not found ", new HttpHeaders(), HttpStatus.NOT_FOUND);
        updateProduct.setCode(product.getCode());
        updateProduct.setOriginalName(product.getOriginalName());
        updateProduct.setProducer(product.getProducer());
        repository.save(updateProduct);
        return new ResponseEntity("Successfully updated ", new HttpHeaders(), HttpStatus.OK);
    }

    @DeleteMapping("/api/products/{id}")
    public ResponseEntity deleteProduct(@PathVariable("id") int id) {
        Product product = repository.getById(id);
        if (product == null) return new ResponseEntity("Product not found ", new HttpHeaders(), HttpStatus.NOT_FOUND);
        repository.delete(product);
        return new ResponseEntity("Successfully deleted ", new HttpHeaders(), HttpStatus.OK);
    }
}
