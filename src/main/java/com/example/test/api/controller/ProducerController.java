package com.example.test.api.controller;

import com.example.test.api.model.Producer;
import com.example.test.api.repository.ProducerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ProducerController {
    @Autowired
    ProducerRepository repository;

    @GetMapping("/api/producers")
    public Iterable<Producer> getProducers() {
        return repository.findAll();
    }

    @PostMapping("/api/producers")
    public ResponseEntity createProducers(@RequestBody Producer producer) {
        repository.save(producer);
        return new ResponseEntity("Successfully created ", new HttpHeaders(), HttpStatus.CREATED);
    }

    @PutMapping("/api/producers/{id}")
    public ResponseEntity update(@PathVariable int id, @RequestBody Producer producer) {
        Producer updateProducer = repository.getById(id);
        if (updateProducer == null)
            return new ResponseEntity("Producer not found ", new HttpHeaders(), HttpStatus.NOT_FOUND);
        updateProducer.setFullName(producer.getFullName());
        updateProducer.setInn(producer.getInn());
        updateProducer.setKpp(producer.getKpp());
        updateProducer.setShortName(producer.getShortName());
        repository.save(updateProducer);
        return new ResponseEntity("Successfully updated ", new HttpHeaders(), HttpStatus.OK);
    }

    @DeleteMapping("/api/producers/{id}")
    public ResponseEntity delete(@PathVariable int id) {
        Producer producer = repository.getById(id);
        if (producer == null) return new ResponseEntity("Producer not found ", new HttpHeaders(), HttpStatus.NOT_FOUND);
        repository.deleteById(id);
        return new ResponseEntity("Successfully deleted ", new HttpHeaders(), HttpStatus.OK);
    }
}