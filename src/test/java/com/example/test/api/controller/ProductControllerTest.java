package com.example.test.api.controller;

import com.example.test.api.model.Producer;
import com.example.test.api.model.Product;
import com.example.test.api.repository.ProductRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ProductController.class)
class ProductControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductRepository repository;

    @Test
    void getProducts() throws Exception {
        Product product = new Product();
        product.setId(1);
        product.setCode("1111");
        product.setOriginalName("yahoo");
        Producer producer = new Producer();
        producer.setId(1);
        producer.setShortName("s1");
        producer.setKpp("kpp");
        producer.setInn("inn");
        producer.setFullName("sssss");
        product.setProducer(producer);
        Product product1 = new Product();
        product1.setId(2);
        product1.setCode("1112");
        product1.setOriginalName("yahoo1");
        product1.setProducer(producer);
        when(repository.findAll()).thenReturn(Arrays.asList(product, product1));
        mockMvc.perform(get("/api/products"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[*].id", containsInAnyOrder(1, 2)))
                .andExpect(jsonPath("$[*].code", containsInAnyOrder("1111", "1112")))
                .andExpect(jsonPath("$[*].producer.id", containsInAnyOrder(1, 1)))
                .andExpect(jsonPath("$[*].originalName", containsInAnyOrder("yahoo", "yahoo1")));
    }

    @Test
    void createProducts() throws Exception {
        Product product = new Product();
        product.setId(1);
        product.setCode("1111");
        product.setOriginalName("yahoo");
        Producer producer = new Producer();
        producer.setId(1);
        producer.setShortName("s1");
        producer.setKpp("kpp");
        producer.setInn("inn");
        producer.setFullName("sssss");
        product.setProducer(producer);
        when(repository.save(product)).thenReturn(product);
        mockMvc.perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(product)))
                .andExpect(status().isCreated());
    }

    @Test
    void updateProduct() throws Exception {
        Product product = new Product();
        product.setId(1);
        product.setCode("1111");
        product.setOriginalName("yahoo");
        Producer producer = new Producer();
        producer.setId(1);
        producer.setShortName("s1");
        producer.setKpp("kpp");
        producer.setInn("inn");
        producer.setFullName("sssss");
        product.setProducer(producer);
        Product product1 = new Product();
        product1.setId(2);
        product1.setCode("1112");
        product1.setOriginalName("yahoo1");
        product1.setProducer(producer);
        when(repository.getById(anyInt())).thenReturn(product1);
        when(repository.save(product1)).thenReturn(product);
        mockMvc.perform(put("/api/products/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(product)))
                .andExpect(status().isOk());
    }

    @Test
    void updateProductNotFound() throws Exception {
        Product product = new Product();
        product.setId(1);
        product.setCode("1111");
        product.setOriginalName("yahoo");
        Producer producer = new Producer();
        producer.setId(1);
        producer.setShortName("s1");
        producer.setKpp("kpp");
        producer.setInn("inn");
        producer.setFullName("sssss");
        product.setProducer(producer);
        when(repository.getById(anyInt())).thenReturn(null);
        mockMvc.perform(put("/api/products/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(product)))
                .andExpect(status().isNotFound());
    }

    @Test
    void deleteProduct() throws Exception {
        Product product = new Product();
        product.setId(1);
        product.setCode("1111");
        product.setOriginalName("yahoo");
        Producer producer = new Producer();
        producer.setId(1);
        producer.setShortName("s1");
        producer.setKpp("kpp");
        producer.setInn("inn");
        producer.setFullName("sssss");
        product.setProducer(producer);
        when(repository.getById(anyInt())).thenReturn(product);
        mockMvc.perform(delete("/api/products/1"))
                .andExpect(status().isOk());
    }

    @Test
    void deleteProductNotFound() throws Exception {
        Product product = new Product();
        product.setId(1);
        product.setCode("1111");
        product.setOriginalName("yahoo");
        Producer producer = new Producer();
        producer.setId(1);
        producer.setShortName("s1");
        producer.setKpp("kpp");
        producer.setInn("inn");
        producer.setFullName("sssss");
        product.setProducer(producer);
        when(repository.getById(anyInt())).thenReturn(null);
        mockMvc.perform(delete("/api/products/1"))
                .andExpect(status().isNotFound());
    }


}