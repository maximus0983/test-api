package com.example.test.api.controller;

import com.example.test.api.model.Producer;
import com.example.test.api.repository.ProducerRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ProducerController.class)
class ProducerControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProducerRepository repository;

    @Test
    void getProducts() throws Exception {
        Producer producer = new Producer();
        producer.setId(1);
        producer.setShortName("s1");
        producer.setKpp("kpp");
        producer.setInn("inn");
        producer.setFullName("ssss");
        Producer producer1 = new Producer();
        producer1.setId(2);
        producer1.setShortName("s2");
        producer1.setKpp("kpp2");
        producer1.setInn("inn2");
        producer1.setFullName("ssss2");
        when(repository.findAll()).thenReturn(Arrays.asList(producer, producer1));
        mockMvc.perform(get("/api/producers"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[*].id", containsInAnyOrder(1, 2)))
                .andExpect(jsonPath("$[*].shortName", containsInAnyOrder("s1", "s2")))
                .andExpect(jsonPath("$[*].kpp", containsInAnyOrder("kpp", "kpp2")))
                .andExpect(jsonPath("$[*].inn", containsInAnyOrder("inn", "inn2")))
                .andExpect(jsonPath("$[*].fullName", containsInAnyOrder("ssss", "ssss2")));

    }

    @Test
    void createProducers() throws Exception {
        Producer producer = new Producer();
        producer.setId(1);
        producer.setShortName("s1");
        producer.setKpp("kpp");
        producer.setInn("inn");
        producer.setFullName("ssss");
        when(repository.save(producer)).thenReturn(producer);
        mockMvc.perform(post("/api/producers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(producer)))
                .andExpect(status().isCreated());
    }

    @Test
    void update() throws Exception {
        Producer producer = new Producer();
        producer.setId(1);
        producer.setShortName("s1");
        producer.setKpp("kpp");
        producer.setInn("inn");
        producer.setFullName("ssss");
        when(repository.getById(anyInt())).thenReturn(producer);
        when(repository.save(producer)).thenReturn(producer);
        mockMvc.perform(put("/api/producers/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(producer)))
                .andExpect(status().isOk());
    }

    @Test
    void updateNotFound() throws Exception {
        Producer producer = new Producer();
        producer.setId(1);
        producer.setShortName("s1");
        producer.setKpp("kpp");
        producer.setInn("inn");
        producer.setFullName("ssss");
        when(repository.getById(anyInt())).thenReturn(null);
        when(repository.save(producer)).thenReturn(producer);
        mockMvc.perform(put("/api/producers/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(producer)))
                .andExpect(status().isNotFound());
    }

    @Test
    void delete() throws Exception {
        Producer producer = new Producer();
        producer.setId(1);
        producer.setShortName("s1");
        producer.setKpp("kpp");
        producer.setInn("inn");
        producer.setFullName("ssss");
        when(repository.getById(anyInt())).thenReturn(producer);
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/producers/1"))
                .andExpect(status().isOk());
    }

    @Test
    void deleteNotFound() throws Exception {
        Producer producer = new Producer();
        producer.setId(1);
        producer.setShortName("s1");
        producer.setKpp("kpp");
        producer.setInn("inn");
        producer.setFullName("ssss");
        when(repository.getById(anyInt())).thenReturn(null);
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/producers/1"))
                .andExpect(status().isNotFound());
    }
}